FROM php:8.2-fpm-alpine

WORKDIR /var/www

RUN apk update && apk upgrade

RUN apk add --no-cache\
    git \
    curl \
    openssl \
    libxml2-dev \
    zlib-dev \
    libzip-dev \
    npm

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install pdo_mysql xml zip

COPY ./ /var/www/

RUN composer update

RUN composer install --optimize-autoloader --no-dev

RUN npm install

RUN php artisan key:generate

RUN php artisan config:cache && php artisan config:clear && php artisan route:cache && php artisan view:cache

EXPOSE 9000

CMD ["php-fpm"]




